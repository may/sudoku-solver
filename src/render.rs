use std::num::NonZeroU32;

use femtovg::{renderer::OpenGl, Canvas};
use glutin::{
    config::ConfigTemplateBuilder,
    context::{ContextAttributesBuilder, PossiblyCurrentContext},
    display::{Display, GetGlDisplay, GlDisplay},
    prelude::NotCurrentGlContext,
    surface::{Surface, SurfaceAttributesBuilder, WindowSurface},
};
use glutin_winit::DisplayBuilder;
use raw_window_handle::HasRawWindowHandle;
use winit::{
    dpi::PhysicalSize,
    event_loop::EventLoop,
    window::{Window, WindowBuilder},
};

pub struct Graphics {
    pub event_loop: EventLoop<()>,
    pub context: PossiblyCurrentContext,
    pub display: Display,
    pub window: Window,
    pub surface: Surface<WindowSurface>,
    pub canvas: Canvas<OpenGl>,
}

impl Graphics {
    pub fn init_window(title: &str) -> Graphics {
        let event_loop = EventLoop::new().unwrap();
        let window_builder = WindowBuilder::new()
            .with_inner_size(PhysicalSize::new(1000.0, 600.0))
            .with_title(title);
        let template = ConfigTemplateBuilder::new().with_alpha_size(8);
        let display_builder = DisplayBuilder::new().with_window_builder(Some(window_builder));
        let (window, gl_config) = display_builder
            .build(&event_loop, template, |mut configs| configs.next().unwrap())
            .unwrap();
        let window = window.unwrap();
        let gl_display = gl_config.display();
        let context_attributes =
            ContextAttributesBuilder::new().build(Some(window.raw_window_handle()));
        let mut not_current_gl_context = Some(unsafe {
            gl_display
                .create_context(&gl_config, &context_attributes)
                .unwrap()
        });
        let attrs = SurfaceAttributesBuilder::<WindowSurface>::new().build(
            window.raw_window_handle(),
            NonZeroU32::new(1000).unwrap(),
            NonZeroU32::new(600).unwrap(),
        );
        let surface = unsafe {
            gl_config
                .display()
                .create_window_surface(&gl_config, &attrs)
                .unwrap()
        };
        let context = not_current_gl_context
            .take()
            .unwrap()
            .make_current(&surface)
            .unwrap();

        let renderer = unsafe {
            OpenGl::new_from_function_cstr(|s| gl_display.get_proc_address(s) as *const _)
                .expect("Cannot create renderer")
        };
        let mut canvas = Canvas::new(renderer).expect("Cannot create canvas");
        canvas.set_size(1000, 600, window.scale_factor() as f32);

        Graphics {
            event_loop,
            context,
            display: gl_display,
            window,
            surface,
            canvas,
        }
    }
}
