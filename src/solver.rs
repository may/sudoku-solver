use rand::{rngs::ThreadRng, thread_rng, Rng};

#[derive(Clone, Copy)]
pub enum Tile {
    Empty,
    Filled(u8),
}

#[derive(Clone, Debug)]
pub enum SolveStep {
    Complete,
    Failed,
    FillTile(usize, usize, u8),
    Choices(usize, usize, Vec<u8>),
}

pub type Board = [[Tile; 9]; 9];

pub trait SudokuBoard {
    fn new(setup: &str) -> Board;
    fn generate(remove_amount: usize) -> Board;
    fn solve(self) -> Option<Board>;
    fn solve_random(self, rng: &mut ThreadRng) -> Option<Board>;
    fn solve_step(self) -> SolveStep;
}

impl Tile {
    pub fn from_char(chr: char) -> Tile {
        match chr {
            '_' => Tile::Empty,
            '1' => Tile::Filled(1),
            '2' => Tile::Filled(2),
            '3' => Tile::Filled(3),
            '4' => Tile::Filled(4),
            '5' => Tile::Filled(5),
            '6' => Tile::Filled(6),
            '7' => Tile::Filled(7),
            '8' => Tile::Filled(8),
            '9' => Tile::Filled(9),
            _ => panic!("Cannot make into tile: `{}`", chr),
        }
    }
    pub fn repr(&self) -> String {
        match self {
            Tile::Empty => " ".to_string(),
            Tile::Filled(num) => format!("{}", num),
        }
    }
}

impl SudokuBoard for Board {
    fn new(setup: &str) -> Board {
        let mut board = [[Tile::Empty; 9]; 9];
        for (y, row) in setup.chars().collect::<Vec<_>>().chunks(9).enumerate() {
            for (x, chr) in row.iter().enumerate() {
                board[y][x] = Tile::from_char(*chr);
            }
        }
        board
    }
    fn solve_step(self) -> SolveStep {
        let mut empty_tiles = vec![];

        for (y, row) in self.into_iter().enumerate() {
            for (x, tile) in row.into_iter().enumerate() {
                if let Tile::Empty = tile {
                    let mut possible_values = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];

                    // Remove all values from the same row
                    for row_tile in row {
                        if let Tile::Filled(num) = row_tile {
                            possible_values.retain(|x| *x != num);
                        }
                    }

                    // Remove all values from the same column
                    for row in self {
                        if let Tile::Filled(num) = row[x] {
                            possible_values.retain(|x| *x != num);
                        }
                    }

                    // Remove all values in the same block
                    let block_x = x / 3;
                    let block_y = y / 3;
                    for (row_index, row) in self.into_iter().enumerate() {
                        if row_index == block_y * 3
                            || row_index == block_y * 3 + 1
                            || row_index == block_y * 3 + 2
                        {
                            for (col_index, tile) in row.into_iter().enumerate() {
                                if col_index == block_x * 3
                                    || col_index == block_x * 3 + 1
                                    || col_index == block_x * 3 + 2
                                {
                                    if let Tile::Filled(num) = tile {
                                        possible_values.retain(|x| *x != num);
                                    }
                                }
                            }
                        }
                    }

                    empty_tiles.push(((x, y), possible_values));
                }
            }
        }

        if empty_tiles.is_empty() {
            return SolveStep::Complete;
        }

        let mut lowest_loc = (0, 0);
        let mut lowest_poss = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];

        for (loc, poss) in empty_tiles {
            if poss.len() < lowest_poss.len() {
                lowest_loc = loc;
                lowest_poss = poss;
            }
        }

        if lowest_poss.is_empty() {
            SolveStep::Failed
        } else if lowest_poss.len() == 1 {
            SolveStep::FillTile(lowest_loc.0, lowest_loc.1, lowest_poss[0])
        } else {
            SolveStep::Choices(lowest_loc.0, lowest_loc.1, lowest_poss)
        }
    }
    fn solve(mut self) -> Option<Board> {
        'solver: loop {
            // clearscreen::clear().unwrap();
            // self.display();
            let mut empty_tiles = vec![];

            for (y, row) in self.into_iter().enumerate() {
                for (x, tile) in row.into_iter().enumerate() {
                    if let Tile::Empty = tile {
                        let mut possible_values = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];

                        // Remove all values from the same row
                        for row_tile in row {
                            if let Tile::Filled(num) = row_tile {
                                possible_values.retain(|x| *x != num);
                            }
                        }

                        // Remove all values from the same column
                        for row in self {
                            if let Tile::Filled(num) = row[x] {
                                possible_values.retain(|x| *x != num);
                            }
                        }

                        // Remove all values in the same block
                        let block_x = x / 3;
                        let block_y = y / 3;
                        for (row_index, row) in self.into_iter().enumerate() {
                            if row_index == block_y * 3
                                || row_index == block_y * 3 + 1
                                || row_index == block_y * 3 + 2
                            {
                                for (col_index, tile) in row.into_iter().enumerate() {
                                    if col_index == block_x * 3
                                        || col_index == block_x * 3 + 1
                                        || col_index == block_x * 3 + 2
                                    {
                                        if let Tile::Filled(num) = tile {
                                            possible_values.retain(|x| *x != num);
                                        }
                                    }
                                }
                            }
                        }

                        empty_tiles.push(((x, y), possible_values));
                    }
                }
            }

            // If there are no items in empty_tiles, then the sudoku
            // is solved and we can return
            if empty_tiles.is_empty() {
                return Some(self);
            }

            let mut lowest_loc = (0, 0);
            let mut lowest_poss = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];

            for (loc, poss) in empty_tiles {
                if poss.len() < lowest_poss.len() {
                    lowest_loc = loc;
                    lowest_poss = poss;
                }
            }

            if lowest_poss.len() == 1 {
                let (x, y) = lowest_loc;
                self[y][x] = Tile::Filled(lowest_poss[0]);
            } else {
                let (x, y) = lowest_loc;
                for poss in lowest_poss {
                    let mut new_board = self;
                    new_board[y][x] = Tile::Filled(poss);
                    if let Some(solution) = new_board.solve() {
                        return Some(solution);
                    }
                }
                break 'solver;
            }
        }
        None
    }
    fn solve_random(mut self, rng: &mut ThreadRng) -> Option<Board> {
        'solver: loop {
            let mut empty_tiles = vec![];

            for (y, row) in self.into_iter().enumerate() {
                for (x, tile) in row.into_iter().enumerate() {
                    if let Tile::Empty = tile {
                        let mut possible_values = vec![1, 2, 3, 4, 5, 6, 7, 8, 9];

                        // Remove all values from the same row
                        for row_tile in row {
                            if let Tile::Filled(num) = row_tile {
                                possible_values.retain(|x| *x != num);
                            }
                        }

                        // Remove all values from the same column
                        for row in self {
                            if let Tile::Filled(num) = row[x] {
                                possible_values.retain(|x| *x != num);
                            }
                        }

                        // Remove all values in the same block
                        let block_x = x / 3;
                        let block_y = y / 3;
                        for (row_index, row) in self.into_iter().enumerate() {
                            if row_index == block_y * 3
                                || row_index == block_y * 3 + 1
                                || row_index == block_y * 3 + 2
                            {
                                for (col_index, tile) in row.into_iter().enumerate() {
                                    if col_index == block_x * 3
                                        || col_index == block_x * 3 + 1
                                        || col_index == block_x * 3 + 2
                                    {
                                        if let Tile::Filled(num) = tile {
                                            possible_values.retain(|x| *x != num);
                                        }
                                    }
                                }
                            }
                        }

                        empty_tiles.push(((x, y), possible_values));
                    }
                }
            }

            // If there are no items in empty_tiles, then the sudoku
            // is solved and we can return
            if empty_tiles.is_empty() {
                return Some(self);
            }

            let mut lowest_loc = vec![(0, 0)];
            let mut lowest_len = 9;
            let mut lowest_poss = vec![vec![1, 2, 3, 4, 5, 6, 7, 8, 9]];

            for (loc, poss) in empty_tiles {
                if poss.len() < lowest_len {
                    lowest_len = poss.len();
                    lowest_loc = vec![loc];
                    lowest_poss = vec![poss];
                } else if poss.len() == lowest_len {
                    lowest_loc.push(loc);
                    lowest_poss.push(poss);
                }
            }

            let choice = rng.gen_range(0..=lowest_loc.len() - 1);
            let lowest_loc = lowest_loc[choice];
            let lowest_poss = lowest_poss.remove(choice);

            if lowest_poss.len() == 1 {
                let (x, y) = lowest_loc;
                self[y][x] = Tile::Filled(lowest_poss[0]);
            } else {
                let (x, y) = lowest_loc;
                for poss in lowest_poss {
                    let mut new_board = self;
                    new_board[y][x] = Tile::Filled(poss);
                    if let Some(solution) = new_board.solve_random(rng) {
                        return Some(solution);
                    }
                }
                break 'solver;
            }
        }
        None
    }
    fn generate(remove_amount: usize) -> Board {
        let board = Board::new(&"_".repeat(81));
        let mut rng = thread_rng();
        let mut solution = board.solve_random(&mut rng).unwrap();
        for _ in 0..remove_amount {
            'gen: loop {
                let x = rng.gen_range(0..=8);
                let y = rng.gen_range(0..=8);
                if let Tile::Filled(_) = solution[y][x] {
                    solution[y][x] = Tile::Empty;
                    break 'gen;
                }
            }
        }
        solution
    }
}
