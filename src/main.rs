use std::{num::NonZeroU32, time::Duration};

use femtovg::{Color, Paint};
use glutin::surface::GlSurface;
use glutin_winit::GlWindow;
use render::Graphics;
use resource::resource;
use solver::{Board, SolveStep, SudokuBoard, Tile};
use winit::{
    event::{Event, StartCause, WindowEvent},
    event_loop::ControlFlow,
};

mod render;
mod solver;

const X_PADDING: u32 = 100;
const Y_PADDING: u32 = 100;

fn main() {
    // let puzzle =
    //     "8__________36______7__9_2___5___7_______457_____1___3___1____68__85___1__9____4__";
    // let solution = Board::new(puzzle).solve().unwrap();
    // // solution.display();

    let board = Board::generate(60);

    let mut graphics = Graphics::init_window("Sudoku");

    let font = graphics
        .canvas
        .add_font_mem(&resource!("./SourceSans3-VariableFont_wght.ttf"))
        .unwrap();

    let mut is_done = false;
    let mut branches = vec![board];
    let mut last_changed = (10, 10);
    let mut last_changed_b = (10, 10);

    graphics
        .event_loop
        .run(|event, target| match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => target.exit(),
                WindowEvent::Resized(size) => {
                    graphics.surface.resize(
                        &graphics.context,
                        NonZeroU32::new(size.width).unwrap(),
                        NonZeroU32::new(size.height).unwrap(),
                    );
                    graphics
                        .window
                        .resize_surface(&graphics.surface, &graphics.context);
                    graphics.canvas.set_size(
                        size.width,
                        size.height,
                        graphics.window.scale_factor() as f32,
                    );
                }
                WindowEvent::RedrawRequested => {
                    let window_width = graphics.canvas.width() as u32;
                    let window_height = graphics.canvas.height() as u32;

                    // Fill background
                    graphics
                        .canvas
                        .clear_rect(0, 0, window_width, window_height, Color::white());

                    let x_space = window_width - 2 * X_PADDING;
                    let y_space = window_height - 2 * Y_PADDING;

                    let (top_left_x, top_left_y, size) = if y_space < x_space {
                        let top_left_y = Y_PADDING;
                        let top_left_x = (window_width - window_height) / 2 + Y_PADDING;
                        let size = y_space;
                        (top_left_x, top_left_y, size)
                    } else {
                        let top_left_x = X_PADDING;
                        let top_left_y = (window_height - window_width) / 2 + X_PADDING;
                        let size = x_space;
                        (top_left_x, top_left_y, size)
                    };

                    let cell_size = size / 9;
                    let size = cell_size * 9;

                    // Draw main border around sudoku
                    graphics.canvas.clear_rect(
                        top_left_x - 8,
                        top_left_y - 8,
                        size + 16,
                        size + 16,
                        Color::black(),
                    );

                    // Empty out space for each tile
                    for x in 0..9 {
                        for y in 0..9 {
                            let color = if (x == last_changed.0 && y == last_changed.1) || is_done {
                                Color::rgba(191, 245, 205, 255)
                            } else if x == last_changed_b.0 && y == last_changed_b.1 {
                                Color::rgba(191, 218, 245, 255)
                            } else {
                                Color::white()
                            };
                            graphics.canvas.clear_rect(
                                top_left_x + x * cell_size + 2,
                                top_left_y + y * cell_size + 2,
                                cell_size - 4,
                                cell_size - 4,
                                color,
                            );
                        }
                    }

                    // Draw inner block border lines
                    graphics.canvas.clear_rect(
                        top_left_x + cell_size * 3,
                        top_left_y,
                        8,
                        size,
                        Color::black(),
                    );
                    graphics.canvas.clear_rect(
                        top_left_x + cell_size * 6,
                        top_left_y,
                        8,
                        size,
                        Color::black(),
                    );
                    graphics.canvas.clear_rect(
                        top_left_x,
                        top_left_y + cell_size * 3,
                        size,
                        8,
                        Color::black(),
                    );
                    graphics.canvas.clear_rect(
                        top_left_x,
                        top_left_y + cell_size * 6,
                        size,
                        8,
                        Color::black(),
                    );

                    // Draw in all numbers
                    let mut paint = Paint::color(Color::black());
                    paint.set_font(&[font]);
                    paint.set_font_size(cell_size as f32 * 0.8);
                    for x in 0..9 {
                        for y in 0..9 {
                            let tl_x = (top_left_x + x * cell_size) as f32;
                            let tl_y = (top_left_y + y * cell_size) as f32;
                            let bl_y = tl_y + cell_size as f32;

                            let text = branches[0][y as usize][x as usize].repr();
                            let metrics = graphics
                                .canvas
                                .measure_text(tl_x, bl_y, &text, &paint)
                                .unwrap();

                            let draw_x = tl_x + (cell_size as f32 - metrics.width()) / 2.0;
                            let draw_y = bl_y - (cell_size as f32 - metrics.height()) / 2.0;

                            graphics
                                .canvas
                                .fill_text(draw_x, draw_y, text, &paint)
                                .unwrap();
                        }
                    }

                    graphics.canvas.flush();
                    graphics
                        .surface
                        .swap_buffers(&graphics.context)
                        .expect("Could not swap buffers");

                    // Complete next solve step
                    if !is_done {
                        match branches[0].solve_step() {
                            SolveStep::Complete => {
                                is_done = true;
                            }
                            SolveStep::Failed => {
                                branches.remove(0);
                            }
                            SolveStep::FillTile(x, y, value) => {
                                branches[0][y][x] = Tile::Filled(value);
                                last_changed = (x as u32, y as u32);
                                last_changed_b = (10, 10);
                            }
                            SolveStep::Choices(x, y, choices) => {
                                for choice in choices {
                                    let mut branch_board = branches[0];
                                    branch_board[y][x] = Tile::Filled(choice);
                                    branches.insert(1, branch_board);
                                    last_changed_b = (x as u32, y as u32);
                                    last_changed = (10, 10);
                                }
                                branches.remove(0);
                            }
                        }
                    }

                    target.set_control_flow(ControlFlow::wait_duration(Duration::from_millis(50)));
                }
                _ => {}
            },
            Event::NewEvents(cause) => {
                if let StartCause::ResumeTimeReached { .. } = cause {
                    graphics.window.request_redraw();
                }
            }
            _ => {}
        })
        .unwrap();
}
